import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cards-component',
  templateUrl: './cards-component.component.html',
  styleUrls: ['./cards-component.component.css'],
})
export class CardsComponentComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}

  // Open Modal Function
  openHealthcareManagement() {
    let modal = document.getElementById(
      'healthcareManagementModal'
    ) as HTMLElement;
    modal.style.display = 'block';
  }

  // Closing Modal Function
  closeHealthcareManagement() {
    let modal = document.getElementById(
      'healthcareManagementModal'
    ) as HTMLElement;
    modal.style.display = 'none';
  }

  // Open Modal Function
  openPsychology() {
    let modal = document.getElementById('psychologyModal') as HTMLElement;
    modal.style.display = 'block';
  }

  // Closing Modal Function
  closePsychology() {
    let modal = document.getElementById('psychologyModal') as HTMLElement;
    modal.style.display = 'none';
  }
}
