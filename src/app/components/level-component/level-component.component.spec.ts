import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LevelComponentComponent } from './level-component.component';

describe('LevelComponentComponent', () => {
  let component: LevelComponentComponent;
  let fixture: ComponentFixture<LevelComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LevelComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LevelComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
