import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HeaderComponentComponent } from './components/header-component/header-component.component';
import { MainComponentComponent } from './components/main-component/main-component.component';
import { CardsComponentComponent } from './components/cards-component/cards-component.component';
import { FormComponentComponent } from './components/form-component/form-component.component';
import { QuoteComponentComponent } from './components/quote-component/quote-component.component';
import { LevelComponentComponent } from './components/level-component/level-component.component';
import { FooterComponentComponent } from './components/footer-component/footer-component.component';

@NgModule({
  declarations: [AppComponent, HeaderComponentComponent, MainComponentComponent, CardsComponentComponent, FormComponentComponent, QuoteComponentComponent, LevelComponentComponent, FooterComponentComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
